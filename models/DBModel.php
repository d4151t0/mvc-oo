<?php
abstract class DBModel{
    /**
    * Host del servidor de base de datos
    * @var string
    * @access private
    */      
    private static $db_host;
    /**
    * Usuario de conexión a la base de datos
    * @var string
    * @access private
    */      
    private static $db_user;
    /**
    * Password de conexión a la base de datos
    * @var string
    * @access private
    */    
    private static $db_pass; 
    /**
    * Nombre la base de datos
    * @var string
    * @access protected
    */    
    protected $db_name; 
    /**
    * Consulta que será ejecutada sobre la base de datos
    * @var string
    * @access protected
    */    
    protected $query;
    /**
    * Resultado de la consulta
    * @var array
    * @access protected
    */        
    protected $rows = array();
    /**
    * Objeto conteniendo la conexión, instancia de mysqli
    * @var object
    * @access private
    */        
    private $conexion;
    /**
    * Mensaje asociado a la conexión
    * @var string
    * @access public
    */      
    public $mensaje = 'Done' ;

    # métodos abstractos para que las clases que hereden los puedan implementar

    /**
    * Obtiene un registro de una tabla
    *
    * @return boolean indicando si se rescata o no el registro
    */      
    abstract protected function selectOne($id);
    /**
    * Crea un nuevo registro de una tabla
    *
    * @param array conteniendo los campos que serán agregados
    */        
    abstract protected function insert($data);
    /**
    * Setea un registro de una tabla
    *
    * @param array conteniendo los campos que serán seteados
    */  
    abstract protected function update();
    /**
    * Elimina un registro de una tabla
    *
    * @return boolean indiando el éxito/fracaso de la eliminación
    */      
    abstract protected function delete();

    abstract protected function selectAll();
    
    # Métodos concretos de la clase

    /**
    * Carga los parámetros de la base de datos
    */       
    protected function setParameters(){
        $database = require_once('database.php');
        $this->db_host = $database['host'];
        $this->db_user = $database['user'];
        $this->db_pass = $database['pass'];
        $this->db_name = $database['database'];  
    }

    /**
    * Conectar a la base de datos
    */       
    private function openConnection() {
        $this->conexion = new mysqli($this->db_host, $this->db_user,
        $this->db_pass, $this->db_name);
    }

    /**
    * Desconectar la base de datos
    */      
    private function closeConnection() {
        $this->conexion->close();
    }

    /**
    * Ejecutar un query simple del tipo INSERT, DELETE, UPDATE
    * @return boolean indicando el éxito/fracaso de la operación
    */        
    protected function executeSingleQuery() {
        $resultado = false;
        if($_POST) {
            $this->openConnection();
            $result = $this->conexion->query($this->query);
            $resultado = ($this->conexion->affected_rows>=1);
            $this->closeConnection();
        } else {
            $this->mensaje = 'Metodo no permitido' ;
        }        
        return $resultado;
    }
    
    /**
    * Traer resultados de una consulta en un Array
    */       
    protected function executeQuery() {
        $this->openConnection();
        $result = $this->conexion->query($this->query);        

        while ($this->rows[] = $result->fetch_assoc());
        
        $result->close();
        $this->closeConnection();
        //print_r($this->rows);
        array_pop($this->rows);
        return $this->rows;        
    }    
}  
?>