<?php
class AutorController{
  public function indexAction(){
    $data = array();
    if ($_POST){
      if (array_key_exists('nombre', $_POST)){
        $data['nombre'] = $_POST['nombre'];
      }
      if (array_key_exists('nacimiento', $_POST)){
        $data['nacimiento'] = $_POST['nacimiento'];
      }
      if(array_key_exists('apellido', $_POST)){
            	$data['apellido'] = $_POST['apellido'];
            }
            $data['titulo'] = "Registro agregado"; 
            var_dump($data);
            $object = new AutorModel(); 
            $object->insert($data);
            return new View('view', $data);
        }
        $titulo = "Crea nuevo autor";
        return new View('create', compact('titulo'));
    }

    public function searchAction($id){        
        $object = new AutorModel();        
        if (!$object->selectOne($id)){
            exit('No existe  ' . $id);
        }
        /* Prepara los datos para la vista */        
        $data = array();
        $data['titulo'] = 'Datos de Autor';
        $data['nombre'] = $object->au_nombre;
        $data['nacimiento'] = $object->au_nacimiento;
        $data['apellido'] = $object->au_apellido;

        /* Retorna la vista */
        return new View('view',$data);
    }

    public function listAction(){
        $object = new AutorModel();
        $data['headers'] = ['ID','NOMBRE','NACIMIENTO','APELLIDO'];
        $registros = $object->selectAll();    
        $data['registros'] = $registros; 
        $data['titulo'] = 'Listado de autores';      
        return new View('list', $data);
    }
}