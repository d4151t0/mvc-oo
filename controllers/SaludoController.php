<?php

// Ejemplo de controlador que muestra un saludo

class SaludoController{
    public function indexAction(){
        exit('Principal');
    }

    public function createAction($mensaje){
        //exit('Saludando ' . $mensaje);
        $titulo = 'MVC con OO';

        return new View('saludo',compact('titulo','mensaje'));   
    }
}