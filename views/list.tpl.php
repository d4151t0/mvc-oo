<html>
    <head>
        <title><?= $titulo;?></title>
    </head>
    <body>
        <table>
            <thead>
                <?php
                foreach($headers as $item){
                    echo '<th>' . $item . '</th>';
                }
                ?>
            </thead>
            <tbody>
                <?php
                foreach($registros as $registro){
                    echo '<tr>';
                    foreach($registro as $key => $value){
                        echo '<td>' . $value . '</td>';
                    }
                    echo '</tr>';
                }               
                ?>
            </tbody>
        </table>
    </body>
</html>