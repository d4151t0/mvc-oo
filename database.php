<?php
/**
* Parámetros de configuración de la base de datos
* @return array colección con los datos de la configuración
 */

  return array(
    "driver"    =>"mysql",
    "host"      =>"localhost:3306",
    "user"      =>"duoc",
    "pass"      =>"duoc",
    "database"  =>"duoc",
    "charset"   =>"utf8"
  );