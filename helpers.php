<?php
function view($template, $data = array()){
    extract($data);
    require "views/$template.php";
}

function controller($name){
    if (empty($name)){
        $name = 'home';
    }
    $archivo = "controllers/$name.php";
    if (file_exists($archivo)){
        require $archivo;
    }
    else{
        # No confiamos en el usuario
        header("HTTP/1.0 404 Not found");
        exit("Página no encontrada");        
    }
}