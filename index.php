<?php
/*
* El frontend controller se encarga de
* configurar la app
*/
require 'config.php';
require 'helpers.php';

require 'library/Request.php';
require 'Library/Inflector.php';
require 'Library/Response.php';
require 'Library/View.php';

require 'models/DBModel.php';
require 'models/AutorModel.php';

$url = isset($_GET['url'])?$_GET['url']:'';
$request = new Request($url);

$request->execute();