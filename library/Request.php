<?php


class Request{

    protected $url;

    protected $controller;
    protected $defaultController = 'home';

    protected $action;
    protected $defaultAction = 'index';

    protected $parameters = array();

    public function __construct($url){
        $this->url = $url;
        # Extrae los segmentos de la url
        $segmentos = explode('/', $this->url);

        $this->resolverController($segmentos);
        $this->resolverAction($segmentos);
        $this->resolverParameters($segmentos);
    }

    # Utiliza pasaje de parámetros por referencia
    public function resolverController(&$segmentos){
        $this->controller = array_shift($segmentos);
        if (empty($this->controller)){
            $this->controller = $this->defaultController;
        }
    }

    public function getUrl(){
        return $this->url;
    }

    public function getControllerClassName(){
        return Inflector::camel($this->controller) . 'Controller';
    }

    public function getController(){
        return $this->controller;
    }

    public function getControllerFileName(){
        return 'controllers/' . $this->getControllerClassName() . '.php';
    }

    # Ahora incluye los mètodos para la reconocer la acción
    public function resolverAction(&$segmentos){
        $this->action = array_shift($segmentos);
        if (empty($this->action)){
            $this->action = $this->defaultAction;
        }
    }

    public function getAction(){
        return $this->action;
    }

    public function getActionMethodName(){
        return Inflector::lowerCamel($this->action) . 'Action';
    }

    # Resuelve los parámetros
    public function resolverParameters(&$segmentos){
        $this->parameters = $segmentos;
    }

    public function getParameters(){
        return $this->parameters;
    }

    public function execute(){
        $controllerClassName = $this->getControllerClassName();
        $controllerFileName = $this->getControllerFileName();
        $actionMethodName = $this->getActionMethodName();
        $parameters = $this->getParameters();

        if (!file_exists($controllerFileName)){
            exit('Controlador inexistente ' . $controllerFileName);
        }
        require $controllerFileName;
        $controller = new $controllerClassName();
        $response = call_user_func_array([$controller, $actionMethodName], $parameters);

        $this->executeResponse($response);
        //$controller->$actionMethodName();
    }

    public function executeResponse($response){
        if ($response instanceof Response){
            $response->execute();    
        }
        elseif (is_string($response)){
            echo $response;
        }
        else{
            exit('Respuesta inválida');
        }
    }
}