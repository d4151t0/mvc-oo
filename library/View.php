<?php

class View extends Response{

    protected $template;
    protected $data = array();
    public function __construct($template, $data = array()){
        $this->template = $template;
        $this->data = $data;
    }

    public function getTemplate(){
        return $this->template;
    }

    public function getData(){
        return $this->data;
    }

    public function execute(){ 
        $template = $this->getTemplate();
        $data = $this->getData();       
        call_user_func(function() use($template, $data){
            extract($data);
            require "views/$template.tpl.php";        
        });
        
    }   
}